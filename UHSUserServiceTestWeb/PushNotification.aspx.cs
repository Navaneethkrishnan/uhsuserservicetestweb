﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using UHSUserServiceTestWeb.com.grcdev.dev.iphr.csfwuhs;


namespace UHSUserServiceTestWeb
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public string UserServiceToken
        {
            get
            {
                return ConfigurationManager.AppSettings["SouthamptonUserServiceToken"];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void SendPushNotification(object sender, EventArgs e)
        {

            bool bIsValid = _IsValid(out string errorMessage);
            if (!bIsValid)
            {
                err.Text = errorMessage;
                DevError.Visible = true;
                return;
            }
            else 
            { 
                DevError.Visible = false; 
            }

            Guid personId = new Guid(txtPersonID.Value.Trim());
            Guid recordId = new Guid(txtRecordID.Value.Trim());
            string messagePayload = txtPayload.Value.Trim();
            PushNotificationResult response;

            try
            {
                UHSUserServiceTestWeb.com.grcdev.dev.iphr.csfwuhs.UserService obj = new UserService();
                response = obj.SendNotificationToAllDevices(personId, recordId, messagePayload, UserServiceToken);
            }
            catch (Exception ex)
            {
                txtResponse.Value = ex.ToString();
                return;
            }

            txtResponse.Value = string.Format("Status code: {0},  Message: {1}", response.StatusCode.ToString(), response.Message);
        }

        private bool _IsValid(out string errorMessage)
        {
            errorMessage = string.Empty;

            if (!Guid.TryParse(txtPersonID.Value.Trim(), out Guid personId))
            {
                errorMessage = "Invalid Person ID";
                return false;
            }

            if (!Guid.TryParse(txtRecordID.Value.Trim(), out Guid recordId))
            {
                errorMessage = "Invalid Record ID";
                return false;
            }

            try
            {
                var details = JObject.Parse(txtPayload.Value);

                string message = details["message"].ToString();
                string mobileUriKey = details["mobile-uri-key"].ToString();
            }
            catch
            {
                errorMessage = "Invalid Payload";
                return false;
            }
            return true;
        }
    }
}