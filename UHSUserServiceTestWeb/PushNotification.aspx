﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PushNotification.aspx.cs" Inherits="UHSUserServiceTestWeb.WebForm1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <h1 class="h3 mb-4 text-gray-800">Push notification</h1>
    <form class="user" runat="server">
        <div class="alert alert-danger" id="DevError" runat="server" visible="false">
            <asp:Literal ID="err" runat="server"></asp:Literal>
        </div>
        <div class="form-group">
            <label for="txtApplicationId">Person ID</label>
            <input runat="server" id="txtPersonID" type="text" class="form-control" aria-describedby="emailHelp" placeholder="Example: 564F1C96-8FBE-41E7-81A4-DAB7A62ECCCA">
        </div>
        <div class="form-group">
            <label for="txtEndpoint">Record ID</label>
            <input runat="server" id="txtRecordID" type="text" class="form-control" aria-describedby="emailHelp" placeholder="Example: B5CBA5A4-B0FA-4D5A-ABAA-7B338D647662">
        </div>
        <div class="form-group">
            <label for="txtPayload">Payload</label>
            <textarea class="form-control" id="txtPayload" runat="server" rows="5" placeholder="Example: { &quot;message&quot;: &quot;Allergy notification!&quot;, &quot;mobile-uri-key&quot;: &quot;m-HealthSummary-Allergies&quot;, &quot;outlet&quot;: &quot;secondary&quot;}"></textarea>
        </div>
        <a runat="server" onserverclick="SendPushNotification" href="javascript:;" class="btn btn-primary btn-user btn-block">Send push notification</a>
        <hr>
        <div class="form-group">
            <label for="txtResponse">Push notification service Response</label>
            <textarea class="form-control" id="txtResponse" runat="server" rows="5"></textarea>
        </div>
    </form>
</asp:Content>
